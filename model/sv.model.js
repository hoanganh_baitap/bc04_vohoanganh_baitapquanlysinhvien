
// tao mot lop doi tuong danh cho Sinh Vien
function SinhVien (ma, ten, email, matKhau, toan, ly, hoa) {
    this.id = ma;
    this.name = ten;
    this.email = email;
    this.pass = matKhau;
    this.math = toan;
    this.physics = ly;
    this.chemistry = hoa;
    this.mediumScore = function () {
        return ((this.math*1 + this.physics*1 + this.chemistry*1) / 3).toFixed(3);
        
    }
    

    
}
