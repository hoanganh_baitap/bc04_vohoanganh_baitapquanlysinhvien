



var listStudent = [];
var listStudentJson = localStorage.getItem("List");
if(listStudentJson != null){
    listStudent = JSON.parse(listStudentJson);
    //  map lại array để có function
    for (var index = 0; index < listStudent.length; index++) {
        var student = listStudent[index];
        listStudent[index] = new SinhVien(
          student.id,
          student.name,
          student.email,
          student.pass,
          student.math,
          student.physics,
          student.chemistry,
        );
      }
      renderListStudent(listStudent);
}



// kiểm tra ở ô input, hợp lệ thì mới được thêm sinh viên
function themSV() {
    var newStudent = layThongTinTuForm();
    
    // console.log("newStudent: ", newStudent);
    var isValid = 
        validator.kiemTraRong(newStudent.id, "spanMaSV", "id sinh viên không để trống") &&
        validator.kiemTraDoDai(newStudent.id, "spanMaSV", "id sinh viên phải có 4 - 6 ký tự", 4, 6);
      // kiem tra ten sinh vien
        isValid =
        isValid & 
        validator.kiemTraRong(newStudent.name, "spanTenSV", "tên sinh viên không để trống");
      // kiem tra mat khau
        isValid = 
        validator.kiemTraRong(newStudent.pass, "spanMatKhau", "mật khẩu sinh viên không để trống") &&
        validator.kiemTraDoDai(newStudent.pass, "spanMatKhau", "mật khẩu sinh viên phải có 6 - 8 ký tự", 6, 8);
        // kiem tra email
        isValid &=
        validator.kiemTraRong(newStudent.email, "spanEmailSV", "email sinh viên không để trống") &&
        validator.kiemTraEmail(newStudent.email, "spanEmailSV", "email không hợp lệ");
  
    if(isValid) {
        listStudent.push(newStudent);
        var listStudentJson = JSON.stringify(listStudent);
        localStorage.setItem("List", listStudentJson);
        renderListStudent(listStudent);
    }
    
}


// xóa sinh viên, sau khi xóa thì lưu vào local trả về mảng mới
function xoaSV (id) {
    // console.log("yes");
    var index = timKiemViTri(id, listStudent);
    if(index != -1){
        listStudent.splice(index, 1);
        var listStudentJson = JSON.stringify(listStudent);
        localStorage.setItem("List", listStudentJson);
        renderListStudent(listStudent);
    }
}

// sửa thông tin sinh viên
function suaSV (id) {
    var index = timKiemViTri(id, listStudent);
    if(index != -1){
        var student = listStudent[index];
        // document.getElementById("txtMaSV").disabled = true;
        showThongTinLenForm(student);
        var listStudentJson = JSON.stringify(listStudent);
        localStorage.setItem("List", listStudentJson);
        renderListStudent(listStudent);
        
    }
}

// cập nhật sinh viên sau khi chỉnh sửa
function capNhat () {
    const maSv = document.getElementById("txtMaSV").value;
    const tenSv = document.getElementById("txtTenSV").value;
    const email = document.getElementById("txtEmail").value;
    const matKhau = document.getElementById("txtPass").value;
    const diemToan = document.getElementById("txtDiemToan").value;
    const diemLy = document.getElementById("txtDiemLy").value;
    const diemHoa = document.getElementById("txtDiemHoa").value;

    layThongTinTuForm();
    // document.getElementById("txtMaSV").disabled = false;
    var newStudent = new SinhVien(
        this.id = maSv,
        this.name = tenSv,
        this.email = email,
        this.pass = matKhau,
        this.math = diemToan,
        this.physics = diemLy,
        this.chemistry = diemHoa,
        )
    for ( var i = 0 ; i < listStudent.length; i++ ) {
        console.log('listStudent: ', listStudent);
        if ( listStudent[i].id == newStudent.id ) {
            listStudent[i] = newStudent;
        }
    }
    console.log('listStudent: ', listStudent);
        var listStudentJson = JSON.stringify(listStudent);
        localStorage.setItem("List", listStudentJson);
        renderListStudent(listStudent);

}

// reset ở các ô input
function resetSV() {
    document.getElementById("formQLSV").reset();
}

// search sinh viên
function timSV() {
    // console.log("yes");
    // lay gia tri trong o search sinh vien
    var searchSV = document.getElementById("txtSearch").value;
    console.log('searchSV: ', searchSV);
    searchArr = [];
    // so sanh gia tri tai o search voi danh sach sinh vien da render ngoai giao dien
    listStudent.forEach(student => {

        console.log(student.name.includes(searchSV));

        if(student.name.includes(searchSV)){
            searchArr.push(student);
            
        }
    });
    console.log('searchArr: ', searchArr);
    renderListStudent(searchArr);
}

