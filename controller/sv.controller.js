
// tao ham lay thong tin
function layThongTinTuForm () {
    const maSv = document.getElementById("txtMaSV").value;
    const tenSv = document.getElementById("txtTenSV").value;
    const email = document.getElementById("txtEmail").value;
    const matKhau = document.getElementById("txtPass").value;
    const diemToan = document.getElementById("txtDiemToan").value;
    const diemLy = document.getElementById("txtDiemLy").value;
    const diemHoa = document.getElementById("txtDiemHoa").value;

    document.getElementById("txtMaSV").value = "";
    document.getElementById("txtTenSV").value = "";
    document.getElementById("txtEmail").value = "";
    document.getElementById("txtPass").value = "";
    document.getElementById("txtDiemToan").value = "";
    document.getElementById("txtDiemLy").value = "";
    document.getElementById("txtDiemHoa").value = "";


    return new SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa);
    
}

// in danh sach sinh vien ra giao dien
function renderListStudent (listArr) {
    var contentHTML = "";
    for(var i = 0; i < listArr.length; i++){
        var student = listArr[i];
        // console.log('student: ', student);
        var trContent = `<tr>
        <td>${student.id}</td>
        <td>${student.name}</td>
        <td>${student.email}</td>
        <td>${student.mediumScore()}</td>
        <td>
        <button onclick = "xoaSV('${student.id}')" class = "btn btn-danger">Xóa</button>
        <button onclick = "suaSV('${student.id}')" class = "btn btn-warning">Sửa</button>
        </td>
        
        </tr>`
        contentHTML += trContent;
        // console.log('contentHTML: ', contentHTML);
    }
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

// tim kiem vi tri sinh vien trong mang
function timKiemViTri (id, listStudent) {
    for(var index = 0; index < listStudent.length; index++){
        var student = listStudent[index];
        if(student.id == id){
            return index;
        }
    };
    return -1;
    
}

// in thong tin len form
function showThongTinLenForm (student) {
    document.getElementById("txtMaSV").value = student.id;
    document.getElementById("txtTenSV").value = student.name;
    document.getElementById("txtEmail").value = student.email;
    document.getElementById("txtPass").value = student.pass;
    document.getElementById("txtDiemToan").value = student.math;
    document.getElementById("txtDiemLy").value = student.physics;
    document.getElementById("txtDiemHoa").value = student.chemistry;

}